# coding: utf-8
import requests
from bs4 import BeautifulSoup

# Json file form:
#
#   '0101': {
#       'saint': [
#           'jean',
#           'charles'
#       ],
#       'proverb': 'A rolling stone gathers no moss'
#   },
#   etc....
#

# btw, '0101' correspond to the January 1st, '0102' to the January 2nd, etc..

# in the url, the months are written like that, so i made an array to browse all the possibilities.
nameMonth = [
    'J',
    'F',
    'M',
    'A',
    'Mai',
    'Juin',
    'Juil',
    'Ao',
    'S',
    'O',
    'N',
    'D'
]

# numMonth variable is for the json file, the same as numDay.
numMonth = 1

# we create, if it's already not done, the json file
txt = open("ProverbDatabase.json", 'w')

# we start the with a '{' (that the json syntax)
txt.write('{')

# for each values in the array above
for stringMonth in nameMonth:

    # we concatenate the link with the value in the array
    url = 'http://www.letoileauxsecrets.fr/Saints-Dictons/Saints-Dictons-'
    url += stringMonth + ".html"

    # we initialize the variable numDay so, at each beginning of a month, the variable go back to 1
    numDay =  1

    # to prepare the string for the month + day concatenate for the json file (exemple 0101)
    if len(str(numMonth)) == 1:
        stringnumMonth = "0"+str(numMonth)
    else:
        stringnumMonth = str(numMonth)

    #we make a requests to the url
    req = requests.get(url)

    # we parse the content to lxml
    soup = BeautifulSoup(req.text, "lxml")

    # for each values in the descendants of the table which has the 'search' id (descendants means all the main tag in the tag we use in find())
    # exemple :
    #
    #   <table id='search'>
    #       <thead>
    #           .......
    #       </thead>
    #
    #       <tbody>
    #           .......
    #       </tbody>
    #   </table>
    #
    #
    # Will return an array with the <thead> content and the <tbody> content.
    #
    for s in soup.find('table', {'id':'search'}).descendants:
        # here we're looking for <td> tag
        if s.name == "td":
            # s.text correspond to the text content of the 's' tag (here <td> cause of the first if)
            if s.text == str(numDay):

                if len(str(numDay)) == 1:
                    stringnumDay = "0"+str(numDay)
                else:
                    stringnumDay = str(numDay)

                # for the rest, read the bs4 documentation : https://www.crummy.com/software/BeautifulSoup/bs4/doc/

                table = s.find_next('td').text.split('\n')
                if len(table)>1:
                    if table[len(table)-1] == '':
                        del table[len(table)-1]
                proverbs = s.find_next('td').find_next('td').text
                proverbs = proverbs.replace("’", "'")
                if proverbs[-1:] == ' ':
                    proverbs = proverbs[:-1]
                if proverbs[-1:] != '.' and proverbs[-1:] != '!' and proverbs[-1:] != '?':
                    proverbs += '.'

                # here we write into the json file with the good syntax (accolade when it's needed, comma when it's needed too etc...)
                # take a look here : https://www.w3schools.com/whatis/whatis_json.asp                
                txt.write('\n\"' + stringnumMonth + stringnumDay + "\":{\n")
                txt.write('\"saint\":[\n')
                for i in range(len(table)):
                    if i == len(table)-1 :
                        txt.write('\"' + table[i] + '\"\n')
                    else:
                        txt.write('\"' + table[i] + '\",\n')

                txt.write('],\"proverb\":\"' + proverbs + "\"\n},")
                numDay += 1
    numMonth += 1
txt.write('}')
