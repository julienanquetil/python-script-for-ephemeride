import os
import csv
import requests
from bs4 import BeautifulSoup
import time

def getEventOfDay(day):
    numeroMois = {
        '01': 'janvier',
        '02': 'février',
        '03': 'mars',
        '04': 'avril',
        '05': 'mai',
        '06': 'juin',
        '07': 'juillet',
        '08': 'août',
        '09': 'septembre',
        '10': 'octobre',
        '11': 'novembre',
        '12': 'décembre'
    }
    url = 'https://www.herodote.net/almanach-jour-'
    url = url+day+'.php'
    req = requests.get(url)
    soup = BeautifulSoup(req.text, "lxml")

    if "0" not in day[-2:]:
        jourAjd = day[-2:] + " " + numeroMois[day[:2]]
    else:
        if day[-1:] == '1':
            jourAjd = day[-1:] + "er  " + numeroMois[day[:2]]
        else:
            jourAjd = day[-1:] + " " + numeroMois[day[:2]]

    titre = []
    for titreEvent in soup.findAll('h3'):
        if jourAjd in titreEvent.text:
            titre.append(titreEvent.text)

    texte = []
    for Event in soup.find("div",{'class':"post-content"}).descendants:
        if jourAjd in Event and ' : ' in Event:
            texte.append(Event.find_next("p").text)

    s =''
    allEventOfTheDay = '['
    i=0
    while i<3 and i<len(titre):
        s = '{\n\"title\":\"'+titre[i]+"\",\n\"event\": \""+texte[i]+"\"},"
        allEventOfTheDay += s
        i+=1
    allEventOfTheDay = allEventOfTheDay[:-1]
    allEventOfTheDay += '],'
    return allEventOfTheDay


txt = open('eventDataBase2.json','w')
txt.write("{")
with open("eventDataBase2.json","w"):
    m=1
    d=1
    mday = [31,28,31,30,31,30,31,31,30,31,30,31]
    while m<=12 :
        day = ''
        if m < 10:
            day = '0' + str(m)
        else:
            day = str(m)
        if d < 10:
            day = day + '0' + str (d)
        else:
            day = day + str(d)
        time.sleep(0.1)
        s = getEventOfDay(day)
        #print ('jour :'+ str(d)+ ' mois :'+ str(m) + s)
        txt.write("\""+day + '\":' + s +'\n')
        if d == mday[m-1]:
            d = 1
            m+=1
        else:
            d+=1

txt.write("}")
